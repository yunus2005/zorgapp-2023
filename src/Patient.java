import java.sql.SQLOutput;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

class Patient
{
   static final int RETURN      = 0;
   static final int SURNAME     = 1;
   static final int FIRSTNAME   = 2;
   static final int DATEOFBIRTH = 3;
   static final int WEIGHT      = 4;
   static final int LENGTH      = 5;

   int       id;
   String    surname;
   String    firstName;
   LocalDate dateOfBirth;
   String formattedDateOfBirth;
   LocalDate currentDate;
   MedicineList medicineList = new MedicineList();
   long age;
   double weight;
   double length;
   double bmi;
   String bmiText;


   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   String getSurname() { return surname; }


   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   String getFirstName()
   {
      return firstName;
   }

   ////////////////////////////////////////////////////////////////////////////////
   // Constructor
   ////////////////////////////////////////////////////////////////////////////////
   Patient( int id, String surname, String firstName, LocalDate dateOfBirth, double weight, double length )
   {
      this.id          = id;
      this.surname     = surname;
      this.firstName   = firstName;
      this.dateOfBirth = dateOfBirth;
      currentDate      = LocalDate.now();
      age              = calcAge();
      this.length      = length;
      this.weight      = weight;
      bmi              = calcBMI();
      setTextBMI(bmi);
      showDateOfBirth(this.dateOfBirth);
   }
   ////////////////////////////////////////////////////////////////////////////////
   ////////////////////////////////////////////////////////////////////////////////
   public void addMedicine( Medicine medicine )
   {
      medicineList.addMedicine( medicine );
   }
   public void wait(int s){
      try{
         TimeUnit.SECONDS.sleep(s);
      }catch (InterruptedException ex){
         Thread.currentThread().interrupt();
      }
   }
   ////////////////////////////////////////////////////////////////////////////////
   // Display patient data.
   ////////////////////////////////////////////////////////////////////////////////
   void viewData()
   {
      System.out.format( "===== Patient id=%d ==============================\n", id );
      System.out.format( "%-17s %s\n", "Surname:", surname );
      System.out.format( "%-17s %s\n", "FirstName:", firstName );
      System.out.format( "%-17s %s\n", "Date of birth:", formattedDateOfBirth );
      System.out.format( "%-17s %s\n", "Age:", age );
      System.out.format( "%-17s %s\n", "Weight:", weight + " kg ");
      System.out.format( "%-17s %s\n", "Length:", length + " m " );
      System.out.format( "%-17s %.2f\n", "Bmi:", bmi );
      System.out.format( "%-17s %s\n", "BMI status: ", bmiText );
      System.out.println("Medicinelist");
      medicineList.write();
      wait(10);

   }

   public void setLength(double length) {
      this.length = length;
   }

   public void setWeight(double weight) {
      this.weight = weight;
   }

   public int getId() {
      return id;
   }

   public void setDateOfBirth(String dateOfBirth) {
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      this.dateOfBirth = LocalDate.from(dtf.parse(dateOfBirth));
   }
   public void showDateOfBirth(LocalDate dateOfBirth){
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      formattedDateOfBirth = dateOfBirth.format(formatter);
   }
   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public void setSurname(String surname) {
      this.surname = surname;
   }

   void editData(){
      var scanner = new BScanner(System.in);
      boolean nextCycle2 = true;
      while (nextCycle2) {
         System.out.format("%d: Return\n", RETURN);
         System.out.format("%d: Surname\n", SURNAME);
         System.out.format("%d: Firstname\n", FIRSTNAME);
         System.out.format("%d: Date of birth\n", DATEOFBIRTH);
         System.out.format("%d: Weight\n", WEIGHT);
         System.out.format("%d: Length\n", LENGTH);

         System.out.println("enter #choice: ");
         int change = scanner.nextInt();
         switch (change) {

            case RETURN:
               nextCycle2 = false;
               break;

            case SURNAME:
               System.out.println("Type new surname: ");
               var editScannerSurname = new BScanner(System.in);
               setSurname(editScannerSurname.nextLine());
               break;

            case FIRSTNAME:
               System.out.println("Type new firstname: ");
               var editScannerFirstname = new BScanner(System.in);
               setFirstName(editScannerFirstname.nextLine());
               break;

            case DATEOFBIRTH:
               System.out.println("Type new Date of birth (yyy-MM-dd): ");
               var editScannerDateOfBirth = new BScanner(System.in);
               setDateOfBirth(editScannerDateOfBirth.nextLine());
               this.age = calcAge();
               checkDateOfBirth();
               break;

            case WEIGHT:
               System.out.print("Type new weight in kilogram (00:00): ");
               var editScannerWeigth = new BScanner(System.in);
               setWeight(editScannerWeigth.nextDouble());
               this.bmi = calcBMI();
               setTextBMI(bmi);
               break;

            case LENGTH:
               System.out.println("Type new length in meters (0.00):");
               var editScannerLength = new BScanner(System.in);
               setLength(editScannerLength.nextDouble());
               this.bmi = calcBMI();
               setTextBMI(bmi);
               break;

            default:
               System.out.println("Please enter a *valid* digit");
               break;
         }
      }
   }
   long calcAge(){ return ChronoUnit.YEARS.between(this.dateOfBirth, currentDate);}
   double calcBMI (){ return this.weight/ (this.length * this.length);}

   public void checkDateOfBirth(){
      if (this.age < 0) {
         var editDOBScanner = new BScanner(System.in);
         System.out.print("Please type a date that's in the past (yyyy-MM-dd): ");
         setDateOfBirth(editDOBScanner.nextLine());
         this.age = calcAge();
         checkDateOfBirth();
      } else {
         System.out.println("You changed the date of birth");
      }
   }
   public void setTextBMI(double boMaIn) {
      this.bmi = boMaIn;
      if (this.age >= 70) {
         if (boMaIn <= 21.9) {
            bmiText = "This person has a slim bmi status";
         } else if (boMaIn <= 28) {
            bmiText = "This person has a good bmi status";
         } else if (boMaIn <= 29.9) {
            bmiText = "This person has a heavy bmi status";
         } else {
            bmiText = "This person has a too heavy bmi status";
         }
      } else {
         if (boMaIn <= 16.9) {
            bmiText = "This person has a too slim bmi status";
         } else if (boMaIn <= 18.4) {
            bmiText = "This person has a slim bmi status";
         } else if (boMaIn <= 25) {
            bmiText = "This person has a good bmi status";
         } else if (boMaIn <= 29.9) {
            bmiText = "This person has a heavy bmi status";
         } else {
            bmiText = "This person has a too heavy bmi status";
         }
      }
   }
   public void medicineWrite(){
      medicineList.menuChooseMedication();
   }
   ////////////////////////////////////////////////////////////////////////////////
   // Shorthand for a Patient's full name
   ////////////////////////////////////////////////////////////////////////////////
   String fullName()
   {
      return String.format( "%s %s [%s]", firstName, surname, formattedDateOfBirth );
   }
}
