class User
{
   String userName;
   int    userID;

   ///////////////////////////////////////////////////////////
   // Hier onder staan de getters.
   String getUserName()
   {
      return userName;
   }

   ///////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////
   int getUserID()
   {
      return userID;
   }

   ///////////////////////////////////////////////////////////
   //Dit is de constructor van de user class.in de constructor staan alle dingen die je nodig hebt om een user te maken.
   public User( int id, String name )
   {
      this.userID   = id;
      this.userName = name;
   }
}
