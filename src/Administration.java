import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

///////////////////////////////////////////////////////////////////////////
// class Administration represents the core of the application by showing
// the main menu, from where all other functionality is accessible, either
// directly or via sub-menus.
//
// An Adminiatration instance needs a User as input, which is passed via the
// constructor to the data member 'çurrentUser'.
// The patient data is available via the data member çurrentPatient.
/////////////////////////////////////////////////////////////////
class Administration
{
   static final int STOP = 0;
   static final int VIEW = 1;
   static final int CHANGE = 2;
   static final int EDIT = 3;
   static final int ALLMEDINCINES = 4;
   static final int EDITMEDLIST = 5;
   static final int ADDMEDICINE = 6;

   Patient currentPatient;            // The currently selected patient
   //private ArrayList<Patient> patients = new ArrayList<>(5);

   // Fixed list of all available medicines
   static MedicineList allMedicnes = new MedicineList();

   // Setters
   //public void setPatients (ArrayList<Patient> patients) { this.patient = patients; }

   // Getters
   //public ArrayList<Patient> getPatients() { return patients; }

   Patient[] patients;
   Patient patient;
   User    currentUser;               // the current user of the program.
   MedicineList allmedicineList = new MedicineList(); // Fixed list of all available medicines

   /////////////////////////////////////////////////////////////////
   // Constructor
   /////////////////////////////////////////////////////////////////
   public Administration( User user )
   {
      currentUser    = user;

      allmedicineList.addMedicine( new Medicine(1,"Paracetamol", "Pijnstiller"," 2X daagse tabletten 250 MG ") );
      allmedicineList.addMedicine( new Medicine(2,"Morfine", "Pijnstiller"," 2X daagse tabletten 30 MG") );
      allmedicineList.addMedicine( new Medicine(3,"Diclofenac", "Onstekingsremmend"," 3X daagse tabletten 50 MG") );
      allmedicineList.addMedicine( new Medicine(4,"Metoprolol", "Onstekingsremmend"," 4X daagse tabletten 50 MG ") );
      allmedicineList.addMedicine( new Medicine(5,"Ibuprofen", "Pijnstiller"," 2X daagse tabletten 250 MG") );
      //currentPatient = new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.6,1.83 );

      Patient patient1 = new Patient( 1, "Van Puffelen", "Pierre", LocalDate.of( 2000, 2, 29 ), 88.6,1.83 );
      patient1.addMedicine(new Medicine(1,"Paracetamol", "Pijnstiller"," 2X daagse tabletten 250 MG "));
      patient1.addMedicine(new Medicine(2,"Morfine", "Pijnstiller"," 2X daagse tabletten 30 MG") );
      patient1.addMedicine(new Medicine(5,"Ibuprofen", "Pijnstiller"," 2X daagse tabletten 250 MG") );
      //this.patient.add(patient);


      Patient patient2 = new Patient( 2, "De Buurt", "Kees", LocalDate.of( 1999, 3, 20 ), 75,1.68 );
      patient2.addMedicine(new Medicine(3,"Diclofenac", "Onstekingsremmend"," 3X daagse tabletten 50 MG") );
      //this.patient.add(patient);

      Patient patient3 = new Patient( 3, "Achterom", "Piet", LocalDate.of( 1996, 5, 13 ), 99,1.99 );
      patient3.addMedicine(new Medicine(4,"Metoprolol", "Onstekingsremmend"," 4X daagse tabletten 50 MG ") );
      patient3.addMedicine(new Medicine(5,"Ibuprofen", "Pijnstiller"," 2X daagse tabletten 250 MG") );


      Patient patient4 = new Patient( 4, "Jansen", "Rick", LocalDate.of( 2001, 8, 8 ), 60,1.89 );
      patient4.addMedicine(new Medicine(1,"Paracetamol", "Pijnstiller"," 2X daagse tabletten 250 MG "));


      Patient patient5 = new Patient( 5, "Van Rein", "Jelle", LocalDate.of( 2003, 10, 22 ), 72,1.77 );


      patients = new Patient[]{patient1,patient2,patient3,patient4,patient5};
      //currentPatient = patients[2];
      checkImputPatientInt(currentUser);



      System.out.format( "Current user: [%d] %s\n", user.getUserID(), user.getUserName());
   }
   public void wait(int s){
      try{
         TimeUnit.SECONDS.sleep(s);
      }catch (InterruptedException ex){
         Thread.currentThread().interrupt();
      }
   }

   /////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////
   void menu()
   {
      var scanner = new Scanner( System.in );  // User input via this scanner.

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.format( "Current patient: %s\n", currentPatient.fullName() );

         ////////////////////////a
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  STOP\n", STOP );
         System.out.format( "%d:  View patient data\n", VIEW );
         System.out.format( "%d:  Change current patient\n", CHANGE );
         System.out.format( "%d:  Edit patient data\n", EDIT );
         System.out.format( "%d:  View the medicines\n", ALLMEDINCINES );
         System.out.format( "%d:  Edit medicinelist current patient\n", EDITMEDLIST );
         System.out.format( "%d:  Add medicine too current patient\n", ADDMEDICINE );

         ////////////////////////

         System.out.print( "enter #choice: " );
         int choice = scanner.nextInt();
         switch (choice)
         {
            case STOP: // interrupt the loop
               nextCycle = false;
               break;

            case VIEW:
               currentPatient.viewData();
               break;

            case EDIT:
               currentPatient.editData();
               break;

            case CHANGE:
               checkImputPatientInt(currentUser);
               break;

            case ALLMEDINCINES:
               allmedicineList.write();
               wait(10);
               break;

            case EDITMEDLIST:
               currentPatient.medicineWrite();
               break;

            case ADDMEDICINE:
               allmedicineList.showFullMedlist(currentPatient);
               break;

            default:
               System.out.println( "Please enter a *valid* digit" );
               break;
         }
      }
   }
   void checkImputPatientInt(User user){
      System.out.format("%s\n","=".repeat(80));
      System.out.format( "Current user: [%d] %s\n", user.getUserID(), user.getUserName() );
      System.out.format("%s\n","=".repeat(80));
      System.out.println("Select your patient");
      var changeScanner = new BScanner(System.in);
      System.out.format("%s\n","=".repeat(80));
      for (int i = 0; i < patients.length; i++){
         patient = patients[i];
         System.out.println(patient.getId() + " " + patient.fullName());
      }
      System.out.print("Enter patient ID: ");
      int patr = changeScanner.nextInt();
      patr--;
      //System.out.println("here i am ");
      if (patr > patients.length - 1){
         System.out.println("Please enter a *valid* digit");
         checkImputPatientInt(currentUser);
      } else {
         //System.out.println("here i am ");

         patient = patients[patr];
         currentPatient = patient;
      }

   }
}
