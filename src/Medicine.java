import java.util.concurrent.TimeUnit;

public class Medicine
{
    private static final int RETURN = 0;
    private static final int DOSE = 1;
    private final int medId;
    private final String name;
    private       String type;
    private       String dose;

    public String getName() {
        return name;
    }

    public int getMedId() {
        return medId;
    }

    public Medicine(int medId, String name, String type, String dose )
    {
        this.medId = medId;
        this.name = name;
        this.dose = dose;
        this.type = type;

    }

    public String getDose() {
        return dose;
    }

    public String getType() {
        return type;
    }
    public void wait(int s){
        try{
            TimeUnit.SECONDS.sleep(s);
        }catch (InterruptedException ex){
            Thread.currentThread().interrupt();
        }
    }

    public void write(){
        System.out.format("===== Medicine id=%d ================\n", getMedId());
        System.out.format("%-17s %s\n", "Name:", getName());
        System.out.format("%-17s %s\n", "Type:", getType());
        System.out.format("%-17s %s\n", "Dosis:", getDose());

    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public void editMedicine(){
        var scanner = new BScanner(System.in);
        boolean nextCycle2 = true;
        while (nextCycle2) {
            System.out.format("%s\n", "=".repeat(80));
            System.out.println("Select object you want to change: ");

            System.out.format("%d: Return\n", RETURN);
            System.out.format("%d: Dose\n", DOSE);
            System.out.print("enter #choice: ");
            int change = scanner.nextInt();
            switch (change) {

                case RETURN:
                    nextCycle2 = false;
                    break;

                case DOSE:
                    //YOU choose to change the dose
                    System.out.print("Type new dosis: ");
                    var editScannerDose = new BScanner(System.in);
                    setDose (editScannerDose.nextLine());
                    System.out.println("Dose has been changed");
                    wait(1);
                    break;

                default:
                    System.out.println("Please enter a *valid* digit");
                    break;
            }


        }
    }
}
