import java.util.ArrayList;
import java.util.Scanner;

public class MedicineList {

    private ArrayList<Medicine> medicines = new ArrayList<>(6);
    Medicine medication;
    Medicine currentMed;

    // Setters

    public void setMedicines(ArrayList<Medicine> medicines) { this.medicines = medicines; }

    // Getters

    public ArrayList<Medicine> getMedicines() { return medicines; }



    // Constructor

    public MedicineList()
    {


    }
    public void write(){
        for(Medicine medicine:medicines){
            medicine.write();
        }
    }
    public void editMedicineData(Medicine medicine){medicine.editMedicine();}

    public void menuChooseMedication(){
        int count = 1;
        for (Medicine medicine: medicines){
            System.out.println(count+ " " + medicine.getName());
            count++;
        }
        System.out.print("Enter choice:");
        var scanner = new BScanner(System.in);
        int med = scanner.nextInt();
        med--;
        medication = medicines.get(med);
        currentMed = medication;
        System.out.format("%s\n", "=".repeat(80));
        System.out.println(currentMed.getName());
        editMedicineData(currentMed);

    }
    //Shows general medlist and let you select one of them to put in de patient medlist.
    public void showFullMedlist(Patient patient){
        for(Medicine medicine: medicines){
            System.out.println(medicine.getMedId() + " " + medicine.getName());
        }
        System.out.print("Enter choice:");
        var scanner = new BScanner(System.in);
        int med = scanner.nextInt();
        med--;
        medication = medicines.get(med);
        currentMed = medication;
        System.out.format("%s\n", "=".repeat(80));
        System.out.println(currentMed.getName() + " has been added to the medicine list of " + patient.getFirstName() + " " + patient.getSurname());
        patient.addMedicine(currentMed);

    }



    public void addMedicine( Medicine medicine )
    {
        medicines.add( medicine );
    }

}
