import java.util.ArrayList;

class ZorgApp
{
   static ArrayList<User> users = new ArrayList<>();
   public static void main( String[] args )
   {
      User currentUser;
      User           user0           = new User( 1, "Mart ElCamera" );
      User           user1           = new User( 2, "Max Coorts" );
      User           user2           = new User( 3, "Ferry Koelman" );
      User           user3           = new User( 4, "Yunus Boztug" );
      User           user4           = new User( 5, "Murda World" );
      users.add(user0);
      users.add(user1);
      users.add(user2);
      users.add(user3);
      users.add(user4);


      boolean firstCycle = true;
      while (firstCycle){
         checkInputUserInt();

      }



   }
   static void checkInputUserInt(){
      User user;
      System.out.format("%s\n","=".repeat(80));
      System.out.println("Select your user");
      var changeScanner = new BScanner(System.in);
      System.out.format("%s\n","=".repeat(80));
      for (int i = 0; i < users.size(); i++){
         user = users.get(i);
         System.out.println(user.getUserID() + " " + user.getUserName());
      }
      System.out.print("Enter User ID: ");
      int patr = changeScanner.nextInt();
      patr--;
      //System.out.println("here i am ");
      if (patr > users.size() - 1){
         System.out.println("Please enter a *valid* digit");
         checkInputUserInt();
      } else {
         //System.out.println("here i am ");

         user = users.get(patr);
         Administration administration = new Administration( user );

         administration.menu();
      }

   }
}
